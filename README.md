# Sokoban Game SFML

Sokoban Game made in SFML-2.3.2

Project compiled using Visual Studio 2010 32-bit

Controls :
1. Movement - WASD
2. Restart - 'R' key
3. Quit - 'Esc' key

![Gameplay Video](Media/trailer.mp4)

Credits to :
1. Harvest Moon for the Inanimate Object Sprites.
2. Sprite Creator XP for the Player Sprite.