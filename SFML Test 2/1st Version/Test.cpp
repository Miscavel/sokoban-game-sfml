#include <SFML/Graphics.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <cctype>
#include <ctime>
#include <cstdlib>
#include <conio.h>
#include <Windows.h>

class Boulder
{
public :
	sf::RectangleShape boulder;

	void boulderSet(sf::Vector2f position,sf::Vector2f size)
	{
		boulder.setPosition(position);
		boulder.setSize(size);
	}
};


bool collisionCheckUp(float positionX, float positionY,int map[100][100])
{
	int collisionDeciderX, collisionDeciderY, collisionCalcX;
	collisionDeciderY = (positionY / 32);
	collisionDeciderX = (positionX / 32);
	collisionCalcX = positionX - floor(positionX / 32) * 32;
	if(map[collisionDeciderX][collisionDeciderY] == 2 || map[collisionDeciderX][collisionDeciderY] == 3)
	{
		if(collisionCalcX >= 20)
		{
			collisionDeciderX = ceil(positionX / 32);
		}
		else
		{
			collisionDeciderX = floor(positionX / 32);
		}
	}
	else
	{
		if(collisionCalcX >= 4)
		{
			collisionDeciderX = ceil(positionX / 32);
		}
		else
		{
			collisionDeciderX = floor(positionX / 32);
		}
	}

	if(map[collisionDeciderX][collisionDeciderY] == 2 || map[collisionDeciderX][collisionDeciderY] == 3)
	{
		if((positionY - ((collisionDeciderY + 1) * 32)) <= 20)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

bool collisionCheckDown(float positionX, float positionY,int map[100][100])
{
	int collisionDeciderX, collisionDeciderY, collisionCalcX;
	collisionDeciderY = (positionY / 32) + 1;
	collisionDeciderX = (positionX / 32);
	collisionCalcX = positionX - floor(positionX / 32) * 32;
	if(map[collisionDeciderX][collisionDeciderY] == 2 || map[collisionDeciderX][collisionDeciderY] == 3)
	{
		if(collisionCalcX >= 20)
		{
			collisionDeciderX = ceil(positionX / 32);
		}
		else
		{
			collisionDeciderX = floor(positionX / 32);
		}
	}
	else
	{
		if(collisionCalcX >= 4)
		{
			collisionDeciderX = ceil(positionX / 32);
		}
		else
		{
			collisionDeciderX = floor(positionX / 32);
		}
	}

	if(map[collisionDeciderX][collisionDeciderY] == 2 || map[collisionDeciderX][collisionDeciderY] == 3)
	{
		if((positionY - ((collisionDeciderY - 1) * 32)) <= 0.1 || (positionY - ((collisionDeciderY - 1) * 32)) <= 32)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

bool collisionCheckLeft(float positionX, float positionY, int map[100][100])
{
	int collisionDeciderX, collisionDeciderY, collisionCalcY;
	collisionDeciderX = (positionX / 32);
	collisionDeciderY = (positionY / 32);
	collisionCalcY = positionY - floor(positionY / 32) * 32;
	if(map[collisionDeciderX][collisionDeciderY] == 2 || map[collisionDeciderX][collisionDeciderY] == 3)
	{
		if(collisionCalcY >= 20)
		{
			collisionDeciderY = ceil(positionY / 32);
		}
		else
		{
			collisionDeciderY = floor(positionY / 32);
		}
	}
	else
	{
		if(collisionCalcY >= 4)
		{
			collisionDeciderY = ceil(positionY / 32);
		}
		else
		{
			collisionDeciderY = floor(positionY / 32);
		}
	}
			
	if(map[collisionDeciderX][collisionDeciderY] == 2 || map[collisionDeciderX][collisionDeciderY] == 3)
	{
		if((positionX - ((collisionDeciderX + 1) * 32)) <= 0.1 || (positionX - ((collisionDeciderX + 1) * 32)) <= 32)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

bool collisionCheckRight(float positionX, float positionY, int map[100][100])
{
	int collisionDeciderX, collisionDeciderY, collisionCalcY;
	collisionDeciderX = (positionX / 32) + 1;
	collisionDeciderY = (positionY / 32);
	collisionCalcY = positionY - floor(positionY / 32) * 32;
	if(map[collisionDeciderX][collisionDeciderY] == 2 || map[collisionDeciderX][collisionDeciderY] == 3)
	{
		if(collisionCalcY >= 20)
		{
			collisionDeciderY = ceil(positionY / 32);
		}
		else
		{
			collisionDeciderY = floor(positionY / 32);
		}
	}
	else
	{
		if(collisionCalcY >= 4)
		{
			collisionDeciderY = ceil(positionY / 32);
		}
		else
		{
			collisionDeciderY = floor(positionY / 32);
		}
	}
			
	if(map[collisionDeciderX][collisionDeciderY] == 2 || map[collisionDeciderX][collisionDeciderY] == 3)
	{
		if((positionX - ((collisionDeciderX - 1) * 32)) <= 0.1 || (positionX - ((collisionDeciderX - 1) * 32)) <= 32)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

bool collisionBoulderDown(float positionX, float positionY, float boulderX1, float boulderY1, float boulderX2, float boulderY2, float boulderX3, float boulderY3, float boulderX4, float boulderY4)
{
	if(abs(positionX - boulderX1) <= 31.5 && (boulderY1 - positionY) <= 32.5 && (boulderY1 - positionY) >= 0)
	{
		return true;
	}
	else if(abs(positionX - boulderX2) <= 31.5 && (boulderY2 - positionY) <= 32.5 && (boulderY2 - positionY) >= 0)
	{
		return true;
	}
	else if(abs(positionX - boulderX3) <= 31.5 && (boulderY3 - positionY) <= 32.5 && (boulderY3 - positionY) >= 0)
	{
		return true;
	}
	else if(abs(positionX - boulderX4) <= 31.5 && (boulderY4 - positionY) <= 32.5 && (boulderY4 - positionY) >= 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool collisionBoulderUp(float positionX, float positionY, float boulderX1, float boulderY1, float boulderX2, float boulderY2, float boulderX3, float boulderY3, float boulderX4, float boulderY4)
{
	if(abs(positionX - boulderX1) <= 31.5 && (positionY - boulderY1) <= 32.5 && (positionY - boulderY1) >= 0)
	{
		return true;
	}
	else if(abs(positionX - boulderX2) <= 31.5 && (positionY - boulderY2) <= 32.5 && (positionY - boulderY2) >= 0)
	{
		return true;
	}
	else if(abs(positionX - boulderX3) <= 31.5 && (positionY - boulderY3) <= 32.5 && (positionY - boulderY3) >= 0)
	{
		return true;
	}
	else if(abs(positionX - boulderX4) <= 31.5 && (positionY - boulderY4) <= 32.5 && (positionY - boulderY4) >= 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool collisionBoulderLeft(float positionX, float positionY, float boulderX1, float boulderY1, float boulderX2, float boulderY2, float boulderX3, float boulderY3, float boulderX4, float boulderY4)
{
	if(abs(positionY - boulderY1) <= 31.5 && (positionX - boulderX1) <= 32.5 && (positionX - boulderX1) >= 0)
	{
		return true;
	}
	else if(abs(positionY - boulderY2) <= 31.5 && (positionX - boulderX2) <= 32.5 && (positionX - boulderX2) >= 0)
	{
		return true;
	}
	else if(abs(positionY - boulderY3) <= 31.5 && (positionX - boulderX3) <= 32.5 && (positionX - boulderX3) >= 0)
	{
		return true;
	}
	else if(abs(positionY - boulderY4) <= 31.5 && (positionX - boulderX4) <= 32.5 && (positionX - boulderX4) >= 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool collisionBoulderRight(float positionX, float positionY, float boulderX1, float boulderY1, float boulderX2, float boulderY2, float boulderX3, float boulderY3, float boulderX4, float boulderY4)
{
	if(abs(positionY - boulderY1) <= 31.5 && (boulderX1 - positionX) <= 32.5 && (boulderX1 - positionX) >= 0)
	{
		return true;
	}
	else if(abs(positionY - boulderY2) <= 31.5 && (boulderX2 - positionX) <= 32.5 && (boulderX2 - positionX) >= 0)
	{
		return true;
	}
	else if(abs(positionY - boulderY3) <= 31.5 && (boulderX3 - positionX) <= 32.5 && (boulderX3 - positionX) >= 0)
	{
		return true;
	}
	else if(abs(positionY - boulderY4) <= 31.5 && (boulderX4 - positionX) <= 32.5 && (boulderX4 - positionX) >= 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void main()
{
	FreeConsole();
	int map[100][100], boulder_amt, boulderX, boulderY, doorX, doorY, goalX, goalY, playerX, playerY;
	int markX[5], markY[5], markCounter, mark_amount, mark_checked, stage_number = -1, j[5];
	bool collision, collisionBoul, stage_load;
	bool boul1c, boul2c, boul3c, boul4c, boul5c;
	bool pause;
	bool gameover = false;

	sf::Vector2i mapSize, loadCounter;
	
	pause = false;

	stage_load = true;

	mapSize.x = 17;
	mapSize.y = 19;
	
	int timer = 300, timer_minute, timer_second;
	sf::Font textFont;
	sf::String sentence, notif_sentence, stage_sentence;
	sf::Text text, notif_text, restart, stage_text, pause_text, gameover_text, congratulations_text;
	std::ostringstream convert_minute, convert_second, convert_stage;

	timer_minute = timer / 60;
	timer_second = timer - timer_minute * 60;

	convert_minute << timer_minute;
	convert_second << timer_second;

	if(timer_second < 10)
	{
		sentence = "Time : " + convert_minute.str() + ":0" + convert_second.str();
	}
	else
	{
		sentence = "Time : " + convert_minute.str() + ":" + convert_second.str();
	}
	textFont.loadFromFile("Lumberjack.otf");
	text.setFont(textFont);
	text.setCharacterSize(32);
	text.setColor(sf::Color(255,255,255,255));
	text.setPosition(608, 26);
	text.setString(sentence);

	restart.setFont(textFont);
	restart.setCharacterSize(32);
	restart.setColor(sf::Color(255,255,255,255));
	restart.setPosition(620, 218);
	restart.setString("Restart");

	stage_text.setFont(textFont);
	stage_text.setCharacterSize(32);
	stage_text.setColor(sf::Color(255,255,255,255));
	stage_text.setPosition(620, 122);

	pause_text.setFont(textFont);
	pause_text.setCharacterSize(32);
	pause_text.setColor(sf::Color(255,255,255,255));
	pause_text.setPosition(32, 32);
	pause_text.setString("Paused");

	gameover_text.setFont(textFont);
	gameover_text.setCharacterSize(64);
	gameover_text.setColor(sf::Color(255,255,255,255));
	gameover_text.setPosition(96,256);
	gameover_text.setString("Game Over!");

	congratulations_text.setFont(textFont);
	congratulations_text.setCharacterSize(64);
	congratulations_text.setColor(sf::Color(255,255,255,255));
	congratulations_text.setPosition(64, 256);
	congratulations_text.setString("Congratulations!");

	notif_sentence = "Stage 1 \nCompleted!";
	notif_text.setString(notif_sentence);
	notif_text.setPosition(550, 40);
	notif_text.setFont(textFont);
	notif_text.setCharacterSize(40);
	notif_text.setColor(sf::Color(255,255,255,255));

	

	sf::Vector2i screenDimension(800, 600);

	enum pDirection {Down, Left, Right, Up};
	
	float frameCounter,switchFrame,frameSpeed;

	frameCounter = 0;
	switchFrame = 200;
	frameSpeed = 800;

	sf::Clock Clock, Clock2;
	sf::Time Time, Time2;

	sf::Vector2i Source;
	Source.x = 1;
	Source.y = Down;

    sf::RenderWindow Window(sf::VideoMode(screenDimension.x, screenDimension.y),"");
	Window.setTitle("Boulders");

	sf::Texture pTexture, bTexture, gTexture, boulTexture, wallTexture, markTexture, doorTexture, goalTexture, doorOpenedTexture, menuBGTexture, flowerBedTexture;
	sf::Texture riverUpTexture, riverDownTexture, riverURCTexture, riverBLCTexture, decorationHouse1Texture;
	sf::Sprite pSprite;

	pTexture.loadFromFile("p1.png");
	pSprite.setTexture(pTexture);
	pSprite.setPosition(32, 32);

	sf::RectangleShape gTile(sf::Vector2f(32, 32));
	
	gTexture.loadFromFile("grass.png");
	gTile.setTexture(&gTexture);

	sf::RectangleShape wall(sf::Vector2f(32, 32));

	wallTexture.loadFromFile("wstump.png");
	wall.setTexture(&wallTexture);
	
	boulTexture.loadFromFile("boulder.png");

	sf::RectangleShape mark(sf::Vector2f(32, 32));
	markTexture.loadFromFile("mark.png");
	mark.setTexture(&markTexture);

	sf::RectangleShape door(sf::Vector2f(32, 32));
	doorTexture.loadFromFile("doorClosed.png");
	door.setTexture(&doorTexture);

	sf::RectangleShape doorOpened(sf::Vector2f(32, 32));
	doorOpenedTexture.loadFromFile("doorOpened.png");
	doorOpened.setTexture(&doorOpenedTexture);

	sf::RectangleShape goal(sf::Vector2f(32, 32));
	
	goalTexture.loadFromFile("snail.png");
	goal.setTexture(&goalTexture);

	Boulder boulder1, boulder2, boulder3, boulder4, boulder5;
	boulder1.boulder.setTexture(&boulTexture);
	boulder2.boulder.setTexture(&boulTexture);
	boulder3.boulder.setTexture(&boulTexture);
	boulder4.boulder.setTexture(&boulTexture);
	boulder5.boulder.setTexture(&boulTexture);

	boulder4.boulderSet(sf::Vector2f(-500,-500),sf::Vector2f(32,32));
	boulder5.boulderSet(sf::Vector2f(-500,-500),sf::Vector2f(32,32));

	sf::RectangleShape riverUp(sf::Vector2f(32, 32));

	riverUpTexture.loadFromFile("water1.png");
	riverUp.setTexture(&riverUpTexture);

	sf::RectangleShape riverDown(sf::Vector2f(32, 32));

	riverDownTexture.loadFromFile("water2.png");
	riverDown.setTexture(&riverDownTexture);

	sf::RectangleShape riverURC(sf::Vector2f(32, 32));

	riverURCTexture.loadFromFile("water3.png");
	riverURC.setTexture(&riverURCTexture);

	sf::RectangleShape riverBLC(sf::Vector2f(32, 32));

	riverBLCTexture.loadFromFile("water4.png");
	riverBLC.setTexture(&riverBLCTexture);
	
	sf::RectangleShape decorationHouse1(sf::Vector2f(256, 192));

	decorationHouse1Texture.loadFromFile("decorationHouse1.png");
	decorationHouse1.setTexture(&decorationHouse1Texture);
	decorationHouse1.setPosition(sf::Vector2f(544, 412));

	sf::RectangleShape menuBG1(sf::Vector2f(256, 96)), menuBG2(sf::Vector2f(256, 96)), menuBG3(sf::Vector2f(256, 96)), flowerBed(sf::Vector2f(256, 128));

	menuBGTexture.loadFromFile("menubg.png");
	menuBG1.setTexture(&menuBGTexture);
	menuBG1.setPosition(sf::Vector2f(544, 0));
	menuBG2.setTexture(&menuBGTexture);
	menuBG2.setPosition(sf::Vector2f(544, 96));
	menuBG3.setTexture(&menuBGTexture);
	menuBG3.setPosition(sf::Vector2f(544, 192));

	flowerBedTexture.loadFromFile("flowerbed.png");
	flowerBed.setTexture(&flowerBedTexture);
	flowerBed.setPosition(sf::Vector2f(544, 288));

    while (Window.isOpen())
    {
		Time = Clock.getElapsedTime();
		
        sf::Event Event;

		if(stage_load == true)
		{
			loadCounter.x = 0;
			loadCounter.y = 0;

			convert_stage.str("");
			convert_stage.clear();
			convert_stage << int(stage_number + 1);

			stage_sentence = "Stage : " + convert_stage.str();
			stage_text.setString(stage_sentence);

			mark_checked = 0;
			doorOpened.setPosition(sf::Vector2f(-500,-500));

			boul1c = false;
			boul2c = false;
			boul3c = false;
			boul4c = false;
			boul5c = false;
			
			std::ifstream openmap("map1.txt");
			openmap.close();
			std::ifstream openboulder("boulder1.txt");
			openboulder.close();
			std::ifstream opengoal("goal1.txt");
			opengoal.close();

			if(stage_number == -1)
			{
				openmap.open("map0.txt");
				openboulder.open("boulder0.txt");
				opengoal.open("goal0.txt");
			}
			if(stage_number == 0)
			{
				openmap.open("map1.txt");
				openboulder.open("boulder1.txt");
				opengoal.open("goal1.txt");
			}
			else if(stage_number == 1)
			{
				openmap.open("map2.txt");
				openboulder.open("boulder2.txt");
				opengoal.open("goal2.txt");
			}
			else if(stage_number == 2)
			{
				openmap.open("map3.txt");
				openboulder.open("boulder3.txt");
				opengoal.open("goal3.txt");
			}
			else if(stage_number == 3)
			{
				openmap.open("map4.txt");
				openboulder.open("boulder4.txt");
				opengoal.open("goal4.txt");
			}
			else if(stage_number == 4)
			{
				openmap.open("map5.txt");
				openboulder.open("boulder5.txt");
				opengoal.open("goal5.txt");
			}
			else if(stage_number == 5)
			{
				openmap.open("goal.txt");
				openboulder.open("goalbldr.txt");
				opengoal.open("goalgl.txt");
			}

			if(openmap.is_open())
			{
				while(!openmap.eof())
				{
					int tileType;
					openmap >> tileType;
					if(loadCounter.x < mapSize.x)
					{
						map[loadCounter.x][loadCounter.y] = tileType;
						loadCounter.x++;
					}
					else
					{
						loadCounter.x = 0;
						loadCounter.y++;
						map[loadCounter.x][loadCounter.y] = tileType;
						loadCounter.x++;
					}
				}
			}
			openmap.close();

			if(openboulder.is_open())
			{
				openboulder >> boulder_amt;
				
				openboulder >> boulderX;
				openboulder >> boulderY;

				boulder1.boulderSet(sf::Vector2f(boulderX, boulderY),sf::Vector2f(32,32));

				openboulder >> boulderX;
				openboulder >> boulderY;

				boulder2.boulderSet(sf::Vector2f(boulderX, boulderY),sf::Vector2f(32,32));

				openboulder >> boulderX;
				openboulder >> boulderY;

				boulder3.boulderSet(sf::Vector2f(boulderX, boulderY),sf::Vector2f(32,32));

				openboulder >> boulderX;
				openboulder >> boulderY;

				boulder4.boulderSet(sf::Vector2f(boulderX, boulderY),sf::Vector2f(32,32));

				openboulder >> boulderX;
				openboulder >> boulderY;

				boulder5.boulderSet(sf::Vector2f(boulderX, boulderY),sf::Vector2f(32,32));
			}
			openboulder.close();
			
			if(opengoal.is_open())
			{
				opengoal >> playerX;
				opengoal >> playerY;

				pSprite.setPosition(sf::Vector2f(playerX, playerY));

				markCounter = 0;
				opengoal >> goalX;
				opengoal >> goalY;
				goal.setPosition(sf::Vector2f(goalX * 32, goalY * 32));

				opengoal >> doorX;
				opengoal >> doorY;

				opengoal >> mark_amount;
				while(!opengoal.eof() || markCounter <= mark_amount)
				{
					opengoal >> markX[markCounter];
					opengoal >> markY[markCounter];
					markCounter++;
				}
			}
			opengoal.close();
			stage_load = false;
		}

		while (Window.pollEvent(Event))
        {
            if (Event.type == sf::Event::Closed)
			{    
				Window.close();
			}
			if(Event.type == sf::Event::KeyReleased)
			{
				if(Event.key.code == sf::Keyboard::C)
				{
					if(pause == false)
					{
						std::cout << mark_checked << "\n";
						pause = true;
					}
					else
					{
						pause = false;
					}
				}
				if(Event.key.code == sf::Keyboard::R)
				{
					stage_load = true;
					if(stage_number == -1 || stage_number == 5)
					{
					}
					else
					{
						timer = timer - 10;
					}

					if(timer < 0)
					{
						timer = 0;
					}
				}
			}
			if(Event.type = sf::Event::MouseButtonReleased)
			{
				if(Event.mouseButton.button == sf::Mouse::Left && Event.mouseButton.x >= 544 && Event.mouseButton.x <= 800 && Event.mouseButton.y >= 192 && Event.mouseButton.y <= 288)
				{
					stage_load = true;
					timer = timer - 5;
				}
			}
		}
		Window.draw(menuBG1);
		Window.draw(menuBG2);
		Window.draw(menuBG3);
		Window.draw(flowerBed);
		if(pause == false && gameover == false)
		{
			if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
			{
					Source.y = Up;

					collision = collisionCheckUp(pSprite.getPosition().x, pSprite.getPosition().y, map);
					if(collision == true)
					{
					}
					else
					{
						if(abs(pSprite.getPosition().x - (boulder1.boulder.getPosition().x)) <= 18 && (pSprite.getPosition().y - (boulder1.boulder.getPosition().y)) <= 15 && (pSprite.getPosition().y - (boulder1.boulder.getPosition().y) >= 0))
						{
							collision = collisionCheckUp(boulder1.boulder.getPosition().x, boulder1.boulder.getPosition().y, map);
							collisionBoul = collisionBoulderUp(boulder1.boulder.getPosition().x,boulder1.boulder.getPosition().y,boulder2.boulder.getPosition().x,boulder2.boulder.getPosition().y,boulder3.boulder.getPosition().x,boulder3.boulder.getPosition().y,boulder4.boulder.getPosition().x,boulder4.boulder.getPosition().y,boulder5.boulder.getPosition().x,boulder5.boulder.getPosition().y);

							if(collision == true || collisionBoul == true)
							{
							}
							else
							{
								pSprite.move(0, -100 * Time.asSeconds());
								boulder1.boulder.move(0, -100 * Time.asSeconds());
							}
						}
						else if(abs(pSprite.getPosition().x - (boulder2.boulder.getPosition().x)) <= 18 && (pSprite.getPosition().y - (boulder2.boulder.getPosition().y)) <= 15 && (pSprite.getPosition().y - (boulder2.boulder.getPosition().y) >= 0))
						{
							collision = collisionCheckUp(boulder2.boulder.getPosition().x, boulder2.boulder.getPosition().y, map);
							collisionBoul = collisionBoulderUp(boulder2.boulder.getPosition().x,boulder2.boulder.getPosition().y,boulder1.boulder.getPosition().x,boulder1.boulder.getPosition().y,boulder3.boulder.getPosition().x,boulder3.boulder.getPosition().y,boulder4.boulder.getPosition().x,boulder4.boulder.getPosition().y,boulder5.boulder.getPosition().x,boulder5.boulder.getPosition().y);

							if(collision == true || collisionBoul == true)
							{
							}
							else
							{
								pSprite.move(0, -100 * Time.asSeconds());
								boulder2.boulder.move(0, -100 * Time.asSeconds());
							}
						}
						else if(abs(pSprite.getPosition().x - (boulder3.boulder.getPosition().x)) <= 18 && (pSprite.getPosition().y - (boulder3.boulder.getPosition().y)) <= 15 && (pSprite.getPosition().y - (boulder3.boulder.getPosition().y) >= 0))
						{
							collision = collisionCheckUp(boulder3.boulder.getPosition().x, boulder3.boulder.getPosition().y, map);
							collisionBoul = collisionBoulderUp(boulder3.boulder.getPosition().x,boulder3.boulder.getPosition().y,boulder2.boulder.getPosition().x,boulder2.boulder.getPosition().y,boulder1.boulder.getPosition().x,boulder1.boulder.getPosition().y,boulder4.boulder.getPosition().x,boulder4.boulder.getPosition().y,boulder5.boulder.getPosition().x,boulder5.boulder.getPosition().y);

							if(collision == true || collisionBoul == true)
							{
							}
							else
							{
								pSprite.move(0, -100 * Time.asSeconds());
								boulder3.boulder.move(0, -100 * Time.asSeconds());
							}
						}
						else if(abs(pSprite.getPosition().x - (boulder4.boulder.getPosition().x)) <= 18 && (pSprite.getPosition().y - (boulder4.boulder.getPosition().y)) <= 15 && (pSprite.getPosition().y - (boulder4.boulder.getPosition().y) >= 0))
						{
							collision = collisionCheckUp(boulder4.boulder.getPosition().x, boulder4.boulder.getPosition().y, map);
							collisionBoul = collisionBoulderUp(boulder4.boulder.getPosition().x,boulder4.boulder.getPosition().y,boulder2.boulder.getPosition().x,boulder2.boulder.getPosition().y,boulder3.boulder.getPosition().x,boulder3.boulder.getPosition().y,boulder1.boulder.getPosition().x,boulder1.boulder.getPosition().y,boulder5.boulder.getPosition().x,boulder5.boulder.getPosition().y);

							if(collision == true || collisionBoul == true)
							{
							}
							else
							{
								pSprite.move(0, -100 * Time.asSeconds());
								boulder4.boulder.move(0, -100 * Time.asSeconds());
							}
						}
						else if(abs(pSprite.getPosition().x - (boulder5.boulder.getPosition().x)) <= 18 && (pSprite.getPosition().y - (boulder5.boulder.getPosition().y)) <= 15 && (pSprite.getPosition().y - (boulder5.boulder.getPosition().y) >= 0))
						{
							collision = collisionCheckUp(boulder5.boulder.getPosition().x, boulder5.boulder.getPosition().y, map);
							collisionBoul = collisionBoulderUp(boulder5.boulder.getPosition().x,boulder5.boulder.getPosition().y,boulder2.boulder.getPosition().x,boulder2.boulder.getPosition().y,boulder3.boulder.getPosition().x,boulder3.boulder.getPosition().y,boulder4.boulder.getPosition().x,boulder4.boulder.getPosition().y,boulder1.boulder.getPosition().x,boulder1.boulder.getPosition().y);

							if(collision == true || collisionBoul == true)
							{
							}
							else
							{
								pSprite.move(0, -100 * Time.asSeconds());
								boulder5.boulder.move(0, -100 * Time.asSeconds());
							}
						}
						else
						{
							pSprite.move(0, -125 * Time.asSeconds());
						}
					}

					frameCounter = frameCounter + frameSpeed * Clock.restart().asSeconds();
					if(frameCounter >= switchFrame)
					{
						frameCounter = 0;
						Source.x++;
						if(Source.x * 32 >= pTexture.getSize().x)
						{
							Source.x = 0;
						}
					}
			}
			else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
			{
				Source.y = Down;
			
				collision = collisionCheckDown(pSprite.getPosition().x, pSprite.getPosition().y, map);

				if(collision == true)
				{
				}
				else
				{
					if(abs(pSprite.getPosition().x - (boulder1.boulder.getPosition().x)) <= 15 && (pSprite.getPosition().y + 32 - boulder1.boulder.getPosition().y) <= 10 && (pSprite.getPosition().y + 32 - boulder1.boulder.getPosition().y) >= 0)
					{
						collision = collisionCheckDown(boulder1.boulder.getPosition().x, boulder1.boulder.getPosition().y, map);
						collisionBoul = collisionBoulderDown(boulder1.boulder.getPosition().x,boulder1.boulder.getPosition().y,boulder2.boulder.getPosition().x,boulder2.boulder.getPosition().y,boulder3.boulder.getPosition().x,boulder3.boulder.getPosition().y,boulder4.boulder.getPosition().x,boulder4.boulder.getPosition().y,boulder5.boulder.getPosition().x,boulder5.boulder.getPosition().y);

						if(collision == true || collisionBoul == true)
						{
						}
						else
						{
							pSprite.move(0, 100 * Time.asSeconds());
							boulder1.boulder.move(0, 100 * Time.asSeconds());
						}
					}
					else if(abs(pSprite.getPosition().x - (boulder2.boulder.getPosition().x)) <= 15 && (pSprite.getPosition().y + 32 - boulder2.boulder.getPosition().y) <= 10 && (pSprite.getPosition().y + 32 - boulder2.boulder.getPosition().y) >= 0)
					{
						collision = collisionCheckDown(boulder2.boulder.getPosition().x, boulder2.boulder.getPosition().y, map);
						collisionBoul = collisionBoulderDown(boulder2.boulder.getPosition().x,boulder2.boulder.getPosition().y,boulder1.boulder.getPosition().x,boulder1.boulder.getPosition().y,boulder3.boulder.getPosition().x,boulder3.boulder.getPosition().y,boulder4.boulder.getPosition().x,boulder4.boulder.getPosition().y,boulder5.boulder.getPosition().x,boulder5.boulder.getPosition().y);

						if(collision == true || collisionBoul == true)
						{
						}
						else
						{
							pSprite.move(0, 100 * Time.asSeconds());
							boulder2.boulder.move(0, 100 * Time.asSeconds());
						}
					}
					else if(abs(pSprite.getPosition().x - (boulder3.boulder.getPosition().x)) <= 15 && (pSprite.getPosition().y + 32 - boulder3.boulder.getPosition().y) <= 10 && (pSprite.getPosition().y + 32 - boulder3.boulder.getPosition().y) >= 0)
					{
						collision = collisionCheckDown(boulder3.boulder.getPosition().x, boulder3.boulder.getPosition().y, map);
						collisionBoul = collisionBoulderDown(boulder3.boulder.getPosition().x,boulder3.boulder.getPosition().y,boulder2.boulder.getPosition().x,boulder2.boulder.getPosition().y,boulder1.boulder.getPosition().x,boulder1.boulder.getPosition().y,boulder4.boulder.getPosition().x,boulder4.boulder.getPosition().y,boulder5.boulder.getPosition().x,boulder5.boulder.getPosition().y);

						if(collision == true || collisionBoul == true)
						{
						}
						else
						{
							pSprite.move(0, 100 * Time.asSeconds());
							boulder3.boulder.move(0, 100 * Time.asSeconds());
						}
					}
					else if(abs(pSprite.getPosition().x - (boulder4.boulder.getPosition().x)) <= 15 && (pSprite.getPosition().y + 32 - boulder4.boulder.getPosition().y) <= 10 && (pSprite.getPosition().y + 32 - boulder4.boulder.getPosition().y) >= 0)
					{
						collision = collisionCheckDown(boulder4.boulder.getPosition().x, boulder4.boulder.getPosition().y, map);
						collisionBoul = collisionBoulderDown(boulder4.boulder.getPosition().x,boulder4.boulder.getPosition().y,boulder2.boulder.getPosition().x,boulder2.boulder.getPosition().y,boulder3.boulder.getPosition().x,boulder3.boulder.getPosition().y,boulder1.boulder.getPosition().x,boulder1.boulder.getPosition().y,boulder5.boulder.getPosition().x,boulder5.boulder.getPosition().y);

						if(collision == true || collisionBoul == true)
						{
						}
						else
						{
							pSprite.move(0, 100 * Time.asSeconds());
							boulder4.boulder.move(0, 100 * Time.asSeconds());
						}
					}
					else if(abs(pSprite.getPosition().x - (boulder5.boulder.getPosition().x)) <= 15 && (pSprite.getPosition().y + 32 - boulder5.boulder.getPosition().y) <= 10 && (pSprite.getPosition().y + 32 - boulder5.boulder.getPosition().y) >= 0)
					{
						collision = collisionCheckDown(boulder5.boulder.getPosition().x, boulder5.boulder.getPosition().y, map);
						collisionBoul = collisionBoulderDown(boulder5.boulder.getPosition().x,boulder5.boulder.getPosition().y,boulder2.boulder.getPosition().x,boulder2.boulder.getPosition().y,boulder3.boulder.getPosition().x,boulder3.boulder.getPosition().y,boulder4.boulder.getPosition().x,boulder4.boulder.getPosition().y,boulder1.boulder.getPosition().x,boulder1.boulder.getPosition().y);

						if(collision == true || collisionBoul == true)
						{
						}
						else
						{
							pSprite.move(0, 100 * Time.asSeconds());
							boulder5.boulder.move(0, 100 * Time.asSeconds());
						}
					}
					else
					{
						pSprite.move(0, 125 * Time.asSeconds());
					}
				}

				frameCounter = frameCounter + frameSpeed * Clock.restart().asSeconds();
				if(frameCounter >= switchFrame)
				{
					frameCounter = 0;
					Source.x++;
					if(Source.x * 32 >= pTexture.getSize().x)
					{
						Source.x = 0;
					}
				}
			
			}
			else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
			{
				Source.y = Left;
			
				collision = collisionCheckLeft(pSprite.getPosition().x, pSprite.getPosition().y, map);

				if(collision == true)
				{
				}
				else
				{
					if((pSprite.getPosition().x - boulder1.boulder.getPosition().x) <= 20 && (pSprite.getPosition().x - boulder1.boulder.getPosition().x) >= 0  && abs(pSprite.getPosition().y - boulder1.boulder.getPosition().y + 10) <= 15)
					{
						collision = collisionCheckLeft(boulder1.boulder.getPosition().x, boulder1.boulder.getPosition().y, map);
						collisionBoul = collisionBoulderLeft(boulder1.boulder.getPosition().x,boulder1.boulder.getPosition().y,boulder2.boulder.getPosition().x,boulder2.boulder.getPosition().y,boulder3.boulder.getPosition().x,boulder3.boulder.getPosition().y,boulder4.boulder.getPosition().x,boulder4.boulder.getPosition().y,boulder5.boulder.getPosition().x,boulder5.boulder.getPosition().y);
					
						if(collision == true || collisionBoul == true)
						{
						}
						else
						{
							pSprite.move (-100 * Time.asSeconds(), 0);
							boulder1.boulder.move(-100 * Time.asSeconds(), 0);
						}
					}
					else if((pSprite.getPosition().x - boulder2.boulder.getPosition().x) <= 20 && (pSprite.getPosition().x - boulder2.boulder.getPosition().x) >= 0  && abs(pSprite.getPosition().y - boulder2.boulder.getPosition().y + 10) <= 15)
					{
						collision = collisionCheckLeft(boulder2.boulder.getPosition().x, boulder2.boulder.getPosition().y, map);
						collisionBoul = collisionBoulderLeft(boulder2.boulder.getPosition().x,boulder2.boulder.getPosition().y,boulder1.boulder.getPosition().x,boulder1.boulder.getPosition().y,boulder3.boulder.getPosition().x,boulder3.boulder.getPosition().y,boulder4.boulder.getPosition().x,boulder4.boulder.getPosition().y,boulder5.boulder.getPosition().x,boulder5.boulder.getPosition().y);

						if(collision == true || collisionBoul == true)
						{
						}
						else
						{
							pSprite.move(-100 * Time.asSeconds(), 0);
							boulder2.boulder.move(-100 * Time.asSeconds(), 0);
						}
					}
					else if((pSprite.getPosition().x - boulder3.boulder.getPosition().x) <= 20 && (pSprite.getPosition().x - boulder3.boulder.getPosition().x) >= 0  && abs(pSprite.getPosition().y - boulder3.boulder.getPosition().y + 10) <= 15)
					{
						collision = collisionCheckLeft(boulder3.boulder.getPosition().x, boulder3.boulder.getPosition().y, map);
						collisionBoul = collisionBoulderLeft(boulder3.boulder.getPosition().x,boulder3.boulder.getPosition().y,boulder2.boulder.getPosition().x,boulder2.boulder.getPosition().y,boulder1.boulder.getPosition().x,boulder1.boulder.getPosition().y,boulder4.boulder.getPosition().x,boulder4.boulder.getPosition().y,boulder5.boulder.getPosition().x,boulder5.boulder.getPosition().y);

						if(collision == true || collisionBoul == true)
						{
						}
						else
						{
							pSprite.move(-100 * Time.asSeconds(), 0);
							boulder3.boulder.move(-100 * Time.asSeconds(), 0);
						}
					}
					else if((pSprite.getPosition().x - boulder4.boulder.getPosition().x) <= 20 && (pSprite.getPosition().x - boulder4.boulder.getPosition().x) >= 0  && abs(pSprite.getPosition().y - boulder4.boulder.getPosition().y + 10) <= 15)
					{
						collision = collisionCheckLeft(boulder4.boulder.getPosition().x, boulder4.boulder.getPosition().y, map);
						collisionBoul = collisionBoulderLeft(boulder4.boulder.getPosition().x,boulder4.boulder.getPosition().y,boulder1.boulder.getPosition().x,boulder1.boulder.getPosition().y,boulder3.boulder.getPosition().x,boulder3.boulder.getPosition().y,boulder2.boulder.getPosition().x,boulder2.boulder.getPosition().y,boulder5.boulder.getPosition().x,boulder5.boulder.getPosition().y);
					
						if(collision == true || collisionBoul == true)
						{
						}
						else
						{
							pSprite.move (-100 * Time.asSeconds(), 0);
							boulder4.boulder.move(-100 * Time.asSeconds(), 0);
						}
					}
					else if((pSprite.getPosition().x - boulder5.boulder.getPosition().x) <= 20 && (pSprite.getPosition().x - boulder5.boulder.getPosition().x) >= 0  && abs(pSprite.getPosition().y - boulder5.boulder.getPosition().y + 10) <= 15)
					{
						collision = collisionCheckLeft(boulder5.boulder.getPosition().x, boulder5.boulder.getPosition().y, map);
						collisionBoul = collisionBoulderLeft(boulder5.boulder.getPosition().x,boulder5.boulder.getPosition().y,boulder2.boulder.getPosition().x,boulder2.boulder.getPosition().y,boulder3.boulder.getPosition().x,boulder3.boulder.getPosition().y,boulder4.boulder.getPosition().x,boulder4.boulder.getPosition().y,boulder1.boulder.getPosition().x,boulder1.boulder.getPosition().y);

						if(collision == true || collisionBoul == true)
						{
						}
						else
						{
							pSprite.move(-100 * Time.asSeconds(), 0);
							boulder5.boulder.move(-100 * Time.asSeconds(), 0);
						}
					}
					else
					{
						pSprite.move(-125 * Time.asSeconds(), 0);
					}
				}

				frameCounter = frameCounter + frameSpeed * Clock.restart().asSeconds();
				if(frameCounter >= switchFrame)
				{
					frameCounter = 0;
					Source.x++;
					if(Source.x * 32 >= pTexture.getSize().x)
					{
						Source.x = 0;
					}
				}
			
			}
			else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
			{
				Source.y = Right;

				collision = collisionCheckRight(pSprite.getPosition().x, pSprite.getPosition().y, map);
			
				if(collision == true)
				{
				}
				else
				{
					if((pSprite.getPosition().x + 20 - boulder1.boulder.getPosition().x) <= 10 && (pSprite.getPosition().x + 20 - boulder1.boulder.getPosition().x) >= 0 && abs(pSprite.getPosition().y - (boulder1.boulder.getPosition().y - 10)) <= 15)
					{
						collision = collisionCheckRight(boulder1.boulder.getPosition().x, boulder1.boulder.getPosition().y, map);
						collisionBoul = collisionBoulderRight(boulder1.boulder.getPosition().x,boulder1.boulder.getPosition().y,boulder2.boulder.getPosition().x,boulder2.boulder.getPosition().y,boulder3.boulder.getPosition().x,boulder3.boulder.getPosition().y,boulder4.boulder.getPosition().x,boulder4.boulder.getPosition().y,boulder5.boulder.getPosition().x,boulder5.boulder.getPosition().y);
					
						if(collision == true || collisionBoul == true)
						{
						}
						else
						{
							pSprite.move(100 * Time.asSeconds(), 0);
							boulder1.boulder.move(100 * Time.asSeconds(), 0);
						}
					}
					else if((pSprite.getPosition().x + 20 - boulder2.boulder.getPosition().x) <= 10 && (pSprite.getPosition().x + 20 - boulder2.boulder.getPosition().x) >= 0 && abs(pSprite.getPosition().y - (boulder2.boulder.getPosition().y - 10)) <= 15)
					{
						collision = collisionCheckRight(boulder2.boulder.getPosition().x, boulder2.boulder.getPosition().y, map);
						collisionBoul = collisionBoulderRight(boulder2.boulder.getPosition().x,boulder2.boulder.getPosition().y,boulder1.boulder.getPosition().x,boulder1.boulder.getPosition().y,boulder3.boulder.getPosition().x,boulder3.boulder.getPosition().y,boulder4.boulder.getPosition().x,boulder4.boulder.getPosition().y,boulder5.boulder.getPosition().x,boulder5.boulder.getPosition().y);

						if(collision == true || collisionBoul == true)
						{
						}
						else
						{
							pSprite.move(100 * Time.asSeconds(), 0);
							boulder2.boulder.move(100 * Time.asSeconds(), 0);
						}
					}
					else if((pSprite.getPosition().x + 20 - boulder3.boulder.getPosition().x) <= 10 && (pSprite.getPosition().x + 20 - boulder3.boulder.getPosition().x) >= 0 && abs(pSprite.getPosition().y - (boulder3.boulder.getPosition().y - 10)) <= 15)
					{
						collision = collisionCheckRight(boulder3.boulder.getPosition().x, boulder3.boulder.getPosition().y, map);
						collisionBoul = collisionBoulderRight(boulder3.boulder.getPosition().x,boulder3.boulder.getPosition().y,boulder2.boulder.getPosition().x,boulder2.boulder.getPosition().y,boulder1.boulder.getPosition().x,boulder1.boulder.getPosition().y,boulder4.boulder.getPosition().x,boulder4.boulder.getPosition().y,boulder5.boulder.getPosition().x,boulder5.boulder.getPosition().y);

						if(collision == true || collisionBoul == true)
						{
						}
						else
						{
							pSprite.move(100 * Time.asSeconds(), 0);
							boulder3.boulder.move(100 * Time.asSeconds(), 0);
						}
					}
					else if((pSprite.getPosition().x + 20 - boulder4.boulder.getPosition().x) <= 10 && (pSprite.getPosition().x + 20 - boulder4.boulder.getPosition().x) >= 0 && abs(pSprite.getPosition().y - (boulder4.boulder.getPosition().y - 10)) <= 15)
					{
						collision = collisionCheckRight(boulder4.boulder.getPosition().x, boulder4.boulder.getPosition().y, map);
						collisionBoul = collisionBoulderRight(boulder4.boulder.getPosition().x,boulder4.boulder.getPosition().y,boulder2.boulder.getPosition().x,boulder2.boulder.getPosition().y,boulder3.boulder.getPosition().x,boulder3.boulder.getPosition().y,boulder1.boulder.getPosition().x,boulder1.boulder.getPosition().y,boulder5.boulder.getPosition().x,boulder5.boulder.getPosition().y);

						if(collision == true || collisionBoul == true)
						{
						}
						else
						{
							pSprite.move(100 * Time.asSeconds(), 0);
							boulder4.boulder.move(100 * Time.asSeconds(), 0);
						}
					}
					else if((pSprite.getPosition().x + 20 - boulder5.boulder.getPosition().x) <= 10 && (pSprite.getPosition().x + 20 - boulder5.boulder.getPosition().x) >= 0 && abs(pSprite.getPosition().y - (boulder5.boulder.getPosition().y - 10)) <= 15)
					{
						collision = collisionCheckRight(boulder5.boulder.getPosition().x, boulder5.boulder.getPosition().y, map);
						collisionBoul = collisionBoulderRight(boulder5.boulder.getPosition().x,boulder5.boulder.getPosition().y,boulder2.boulder.getPosition().x,boulder2.boulder.getPosition().y,boulder3.boulder.getPosition().x,boulder3.boulder.getPosition().y,boulder4.boulder.getPosition().x,boulder4.boulder.getPosition().y,boulder1.boulder.getPosition().x,boulder1.boulder.getPosition().y);

						if(collision == true || collisionBoul == true)
						{
						}
						else
						{
							pSprite.move(100 * Time.asSeconds(), 0);
							boulder5.boulder.move(100 * Time.asSeconds(), 0);
						}
					}
					else
					{
						pSprite.move(125 * Time.asSeconds(), 0);
					}
				}
			
				frameCounter = frameCounter + frameSpeed * Clock.restart().asSeconds();
				if(frameCounter >= switchFrame)
				{
					frameCounter = 0;
					Source.x++;
					if(Source.x * 32 >= pTexture.getSize().x)
					{
						Source.x = 0;
					}
				}
			}
			else if(sf::Keyboard::isKeyPressed(sf::Keyboard::V))
			{
				std::cout << mark_amount << " " << mark_checked << std::endl;
			}
		}

		Clock.restart();

		if(pause == false && gameover == false)
		{
			Time2 = Clock2.getElapsedTime();
			if(stage_number == -1 || stage_number == 5)
			{
				Clock2.restart();
			}
			else
			{
				if(Time2.asSeconds() >= 1)
				{
					Clock2.restart();
					convert_minute.str("");
					convert_second.str("");
					convert_minute.clear();
					convert_second.clear();

					if(timer > 0)
					{
						timer--;
					}

					timer_minute = timer / 60;
					timer_second = timer - timer_minute * 60;

					convert_minute << timer_minute;
					convert_second << timer_second;

					if(timer_second < 10)
					{
						sentence = "Time : " + convert_minute.str() + ":0" + convert_second.str();
					}
					else
					{
						sentence = "Time : " + convert_minute.str() + ":" + convert_second.str();
					}
					text.setString(sentence);
				}
			}
		}

		if(timer == 0)
		{
			gameover = true;
		}

		pSprite.setTextureRect(sf::IntRect(Source.x * 32, Source.y * 32, 32, 32));

		for(int j = 0;j <= loadCounter.y;j++)
		{
			for(int i = 0;i < loadCounter.x;i++)
			{
				if(map[i][j] == 0)
				{
					gTile.setPosition(i * 32, j * 32);
					Window.draw(gTile);
				}
				else if(map[i][j] == 2)
				{
					wall.setPosition(i * 32, j * 32);
					Window.draw(wall);
				}
				else if(map[i][j] == 1)
				{
					mark.setPosition(i * 32, j * 32);
					Window.draw(mark);
				}
				else if(map[i][j] == 3)
				{
					door.setPosition(i * 32, j * 32);
					Window.draw(door);
				}
				else if(map[i][j] == 4)
				{
					doorOpened.setPosition(i * 32, j * 32);
					Window.draw(doorOpened);
				}
				else if(map[i][j] == 7)
				{
					riverUp.setPosition(i * 32, j * 32);
					Window.draw(riverUp);
				}
				else if(map[i][j] == 8)
				{
					riverDown.setPosition(i * 32, j * 32);
					Window.draw(riverDown);
				}
				else if(map[i][j] == 9)
				{
					riverURC.setPosition(i * 32, j * 32);
					Window.draw(riverURC);
				}
				else if(map[i][j] == 10)
				{
					riverBLC.setPosition(i * 32, j * 32);
					Window.draw(riverBLC);
				}
			}
		}
		Window.draw(gTile);
		Window.draw(text);
		for(int i = 1; i <= boulder_amt; i++)
		{
			if(i == 1)
			{
				Window.draw(boulder1.boulder);
			}
			if(i == 2)
			{
				Window.draw(boulder2.boulder);
			}
			if(i == 3)
			{
				Window.draw(boulder3.boulder);
			}
			if(i == 4)
			{
				Window.draw(boulder4.boulder);
			}
			if(i == 5)
			{
				Window.draw(boulder5.boulder);
			}
		}

		for(int i = 0; i < mark_amount ; i++)
		{
			if(boul1c == false)
			{
				if(boulder1.boulder.getPosition().x >= ((markX[i] * 32) - 10) && boulder1.boulder.getPosition().x <= ((markX[i] * 32) + 10) && boulder1.boulder.getPosition().y >= ((markY[i] * 32) - 10) && boulder1.boulder.getPosition().y <= ((markY[i] * 32) + 10))
				{
					j[0] = i;
					mark_checked++;
					boul1c = true;
				}
			}
			
			if(boul1c == true)
			{
				if(boulder1.boulder.getPosition().x < ((markX[j[0]] * 32) - 20) || boulder1.boulder.getPosition().x > ((markX[j[0]] * 32) + 20) || boulder1.boulder.getPosition().y < ((markY[j[0]] * 32) - 20) || boulder1.boulder.getPosition().y > ((markY[j[0]] * 32) + 20))
				{
					mark_checked--;
					boul1c = false;
				}
			}


			if(boul2c == false)
			{
				if(boulder2.boulder.getPosition().x >= ((markX[i] * 32) - 10) && boulder2.boulder.getPosition().x <= ((markX[i] * 32) + 10) && boulder2.boulder.getPosition().y >= ((markY[i] * 32) - 10) && boulder2.boulder.getPosition().y <= ((markY[i] * 32) + 10))
				{
					j[1] = i;
					mark_checked++;
					boul2c = true;
				}
			}
			
			if(boul2c == true)
			{
				if(boulder2.boulder.getPosition().x < ((markX[j[1]] * 32) - 20) || boulder2.boulder.getPosition().x > ((markX[j[1]] * 32) + 20) || boulder2.boulder.getPosition().y < ((markY[j[1]] * 32) - 20) || boulder2.boulder.getPosition().y > ((markY[j[1]] * 32) + 20))
				{
					mark_checked--;
					boul2c = false;
				}
			}


			if(boul3c == false)
			{
				if(boulder3.boulder.getPosition().x >= ((markX[i] * 32) - 10) && boulder3.boulder.getPosition().x <= ((markX[i] * 32) + 10) && boulder3.boulder.getPosition().y >= ((markY[i] * 32) - 10) && boulder3.boulder.getPosition().y <= ((markY[i] * 32) + 10))
				{
					j[2] = i;
					mark_checked++;
					boul3c = true;
				}
			}
			
			if(boul3c == true)
			{
				if(boulder3.boulder.getPosition().x < ((markX[j[2]] * 32) - 20) || boulder3.boulder.getPosition().x > ((markX[j[2]] * 32) + 20) || boulder3.boulder.getPosition().y < ((markY[j[2]] * 32) - 20) || boulder3.boulder.getPosition().y > ((markY[j[2]] * 32) + 20))
				{
					mark_checked--;
					boul3c = false;
				}
			}


			if(boul4c == false)
			{
				if(boulder4.boulder.getPosition().x >= ((markX[i] * 32) - 10) && boulder4.boulder.getPosition().x <= ((markX[i] * 32) + 10) && boulder4.boulder.getPosition().y >= ((markY[i] * 32) - 10) && boulder4.boulder.getPosition().y <= ((markY[i] * 32) + 10))
				{
					j[3] = i;
					mark_checked++;
					boul4c = true;
				}
			}
			
			if(boul4c == true)
			{
				if(boulder4.boulder.getPosition().x < ((markX[j[3]] * 32) - 20) || boulder4.boulder.getPosition().x > ((markX[j[3]] * 32) + 20) || boulder4.boulder.getPosition().y < ((markY[j[3]] * 32) - 20) || boulder4.boulder.getPosition().y > ((markY[j[3]] * 32) + 20))
				{
					mark_checked--;
					boul4c = false;
				}
			}

			if(boul5c == false)
			{
				if(boulder5.boulder.getPosition().x >= ((markX[i] * 32) - 10) && boulder5.boulder.getPosition().x <= ((markX[i] * 32) + 10) && boulder5.boulder.getPosition().y >= ((markY[i] * 32) - 10) && boulder5.boulder.getPosition().y <= ((markY[i] * 32) + 10))
				{
					j[4] = i;
					mark_checked++;
					boul5c = true;
				}
			}

			if(boul5c == true)
			{
				if(boulder5.boulder.getPosition().x < ((markX[j[4]] * 32) - 20) || boulder5.boulder.getPosition().x > ((markX[j[4]] * 32) + 20) || boulder5.boulder.getPosition().y < ((markY[j[4]] * 32) - 20) || boulder5.boulder.getPosition().y > ((markY[j[4]] * 32) + 20))
				{
					mark_checked--;
					boul5c = false;
				}
			}
		}

		if(mark_checked == mark_amount)
		{
			map[doorX][doorY] = 4;
		}
		else
		{
			map[doorX][doorY] = 3;
		}

		if(pSprite.getPosition().x >= (doorOpened.getPosition().x - 15) && pSprite.getPosition().x <= (doorOpened.getPosition().x + 6) && pSprite.getPosition().y >= (doorOpened.getPosition().y - 15) && pSprite.getPosition().y <= (doorOpened.getPosition().y + 6))
		{
			stage_load = true;
			stage_number++;
		}

		if(stage_number == 5)
		{
			stage_text.setString("Goal!");
			Window.draw(congratulations_text);
		}

		Window.draw(stage_text);
		Window.draw(restart);
		Window.draw(decorationHouse1);
		Window.draw(goal);
        Window.draw(pSprite);
		if(pause == true)
		{
			Window.draw(pause_text);
		}
		if(gameover == true)
		{
			Window.draw(gameover_text);
		}
		
        Window.display();
		Window.clear();

    }
}


//Backup Codes

//collisionCheckUp
						/*collisionDeciderY1 = (pSprite.getPosition().y / 32);
						collisionDeciderX1 = pSprite.getPosition().x / 32;
						collisionCalcX = pSprite.getPosition().x - floor(pSprite.getPosition().x / 32) * 32;
						if(map[collisionDeciderX1][collisionDeciderY1] == 2)
						{
							if(collisionCalcX >= 20)
							{
								collisionDeciderX1 = ceil(pSprite.getPosition().x / 32);
							}
							else
							{
								collisionDeciderX1 = floor(pSprite.getPosition().x / 32);
							}
						}
						else
						{
							if(collisionCalcX >= 4)
							{
								collisionDeciderX1 = ceil(pSprite.getPosition().x / 32);
							}
							else
							{
								collisionDeciderX1 = floor(pSprite.getPosition().x / 32);
							}
						}
			
						if(map[collisionDeciderX1][collisionDeciderY1] == 2)
						{
							if((pSprite.getPosition().y - ((collisionDeciderY1 + 1) * 32)) <= 0.1 || (pSprite.getPosition().y - ((collisionDeciderY1 + 1) * 32)) <= 32)
							{
							}
							else
							{
								pSprite.move(0, -100 * Time.asSeconds());
							}
						}
						else
						{
							pSprite.move(0, -100 * Time.asSeconds());
						}*/

//collisionCheckDown
						/*collisionDeciderY1 = (pSprite.getPosition().y / 32) + 1;
						collisionDeciderX1 = pSprite.getPosition().x / 32;
						collisionCalcX = pSprite.getPosition().x - floor(pSprite.getPosition().x / 32) * 32;
						if(map[collisionDeciderX1][collisionDeciderY1] == 2)
						{
							if(collisionCalcX >= 20)
							{
								collisionDeciderX1 = ceil(pSprite.getPosition().x / 32);
							}
							else
							{
								collisionDeciderX1 = floor(pSprite.getPosition().x / 32);
							}
						}
						else
						{
							if(collisionCalcX >= 4)
							{
								collisionDeciderX1 = ceil(pSprite.getPosition().x / 32);
							}
							else
							{
								collisionDeciderX1 = floor(pSprite.getPosition().x / 32);
							}
						}

						if(map[collisionDeciderX1][collisionDeciderY1] == 2)
						{
							if((pSprite.getPosition().y - ((collisionDeciderY1 - 1) * 32)) <= 0.1 || (pSprite.getPosition().y - ((collisionDeciderY1 - 1) * 32)) <= 32 )
							{
							}
							else
							{
								pSprite.move(0, 100 * Time.asSeconds());
							}
						}
						else
						{
							pSprite.move(0, 100 * Time.asSeconds());
						}*/

//collisionCheckLeft

						/*collisionDeciderX1 = (pSprite.getPosition().x / 32);
						collisionDeciderY1 = pSprite.getPosition().y / 32;
						collisionCalcY = pSprite.getPosition().y - floor(pSprite.getPosition().y / 32) * 32;
						if(map[collisionDeciderX1][collisionDeciderY1] == 2)
						{
							if(collisionCalcY >= 20)
							{
								collisionDeciderY1 = ceil(pSprite.getPosition().y / 32);
							}
							else
							{
								collisionDeciderY1 = floor(pSprite.getPosition().y / 32);
							}
						}
						else
						{
							if(collisionCalcY >= 4)
							{
								collisionDeciderY1 = ceil(pSprite.getPosition().y / 32);
							}
							else
							{
								collisionDeciderY1 = floor(pSprite.getPosition().y / 32);
							}
						}
			
						if(map[collisionDeciderX1][collisionDeciderY1] == 2)
						{
							if((pSprite.getPosition().x - ((collisionDeciderX1 + 1) * 32)) <= 0.1 || (pSprite.getPosition().x - ((collisionDeciderX1 + 1) * 32)) <= 32)
							{
							}
							else
							{
								pSprite.move(-100 * Time.asSeconds(), 0);
							}
						}
						else
						{
							pSprite.move(-100 * Time.asSeconds(), 0);
						}*/

//collisionCheckRight

						/*collisionDeciderX1 = (pSprite.getPosition().x / 32) + 1;
						collisionDeciderY1 = pSprite.getPosition().y / 32;
						collisionCalcY = pSprite.getPosition().y - floor(pSprite.getPosition().y / 32) * 32;
						if(map[collisionDeciderX1][collisionDeciderY1] == 2)
						{
							if(collisionCalcY >= 20)
							{
								collisionDeciderY1 = ceil(pSprite.getPosition().y / 32);
							}
							else
							{
								collisionDeciderY1 = floor(pSprite.getPosition().y / 32);
							}
						}
						else
						{
							if(collisionCalcY >= 4)
							{
								collisionDeciderY1 = ceil(pSprite.getPosition().y / 32);
							}
							else
							{
								collisionDeciderY1 = floor(pSprite.getPosition().y / 32);
							}
						}

						if(map[collisionDeciderX1][collisionDeciderY1] == 2)
						{
							if((pSprite.getPosition().x - ((collisionDeciderX1 - 1) * 32)) <= 0.1 || (pSprite.getPosition().x - ((collisionDeciderX1 - 1) * 32)) <= 32)
							{
							}
							else
							{
								pSprite.move(100 * Time.asSeconds(), 0);
							}
						}
						else
						{
							pSprite.move(100 * Time.asSeconds(), 0);
						}*/

//movingView

						/*viewPosition.x = pSprite.getPosition().x + 16 - (screenDimension.x / 2);
						viewPosition.y = pSprite.getPosition().y + 16 - (screenDimension.y / 2);

						if(viewPosition.x < 0)
						{
							viewPosition.x = 0;
						}
						if(viewPosition.y < 0)
						{
							viewPosition.y = 0;
						}

						view.reset(sf::FloatRect(viewPosition.x, viewPosition.y, screenDimension.x, screenDimension.y));

						Window.setView(view);*/

//playerAnimation

						/*frameCounter = frameCounter + frameSpeed * Clock.restart().asSeconds();
						if(frameCounter >= switchFrame)
						{
							frameCounter = 0;
							Source.x++;
							if(Source.x * 32 >= pTexture.getSize().x)
							{
								Source.x = 0;
							}
						}*/